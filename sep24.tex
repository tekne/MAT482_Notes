\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT482 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 24 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}
\usepackage{hyperref}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Reg}{reg}
\DeclareMathOperator{\PA}{PA}
\DeclareMathOperator{\LIM}{LIM}

\begin{document}

\maketitle

\begin{center}
Based off handwritten notes by Yasin Mobassir.
\end{center}

\vspace{5mm}

\section*{Ordinals}

We define the following types of ordinals
\begin{definition}
\begin{enumerate}

  \item \textbf{Zero} (\(0\))

  \item \textbf{Successor ordinals}: ordinals of the form \(\alpha + 1\)

  \item \textbf{Limit ordinals:} non-zero ordinals \(\alpha\), where \(\alpha\) is a limit and does not have an immediate predecessor

\end{enumerate}
\end{definition}
All natural numbers are either zero or successor ordinals. From this, we can perform induction on ordinals as follows: let \(P\) be a property. If
\begin{enumerate}

  \item \(P(0)\) is true

  \item \(P(\alpha) \implies P(\alpha + 1)\)

  \item For every limit \(\alpha\), if \(P(\beta)\) holds for every \(\beta < \alpha\) then \(P(\alpha)\)

\end{enumerate}
Then \(P(\alpha)\) is true for every ordinal \(\alpha\).

In ordinal arithmetic, the operations are continuous in the second variable. That is, if \(\beta\) is a limit ordinal \(\beta = \sup\{\beta_i : i \in B\}\), where \(B\) is countable, we have
\begin{itemize}

  \item \(\alpha + \beta = \sup\{\alpha + \beta_i : i \in B\}\)

  \item \(\alpha\beta = \sup\{\alpha\beta_i : i \in B\}\)

  \item \(\alpha^\beta = \sup\{\alpha^{\beta_i} : i \in B\}\)

\end{itemize}
Every countable set of ordinals has a supremum, so these are well-defined. Note that cardinal arithmetic is \textit{not} ordinal arithmetic: we have
\[\omega = \sup\nats, 2^\omega = \sup\{2^n : n \in \nats\} = \omega\]
i.e. there is nothing between \(\omega\) and \(2^\omega\).
\begin{proposition}
  The ordinals are well-ordered. In particular, this means there is no infinitely decreasing series of ordinals.
\end{proposition}
We begin with a remark: let \(\LIM\) denote a \textbf{class of limit numerals}, satisfying
\begin{enumerate}

  \item \(\alpha \in \LIM \iff \alpha_k \neq 0\)

  \item If \(\alpha \neq \epsilon_0, \alpha > \alpha_i\)

\end{enumerate}
Every natural number greater than zero can be expressed in any base, for example,
\[261 = 2^8 + 2^2 + 2^0 = \verb|0b100000101| = 2^{2^3} + 2^2 + 2^0 = 2^{2^{2^1 + 2^0}} + 2^2 + 2^0\]
This last form is called ``pure base \(2\)", or ``hereditary base \(2\)", which requires only \(2\) and \(1\), i.e. \(2^1\) and \(2^0\). For base 3, we can write
\[100 = 3^4 + 2 \cdot 3^2 + 3^0 = 3^{3^{3^1 + 3^0}} + 2 \cdot 3^2 + 3^0\]
Note ``pure base \(3\)" requires only \(3\), \(2\) and \(1\).
Now, for every natural number \(m \in \nats\), we define \textbf{Goodstein's Sequence}
\[L(m) = (L_0(m), L_1(m), ...)\]
by
\begin{itemize}

  \item[(1)] \(L_0(m) = m\)

  \item[(2)] If \(L_0(m) = 0\), stop. Otherwise, \(L_1(m)\) is obtained by
  \begin{enumerate}

    \item Write \(L_0(m)\) in pure base-2

    \item Change all the \(2\)'s into \(3\)'s, and subtract 1.

  \end{enumerate}

  \item [\(\vdots\)]

  \item [\(n + 2\)] If \(L_n(m) = 0\), stop. Otherwise, \(L_{n + 1}(m)\) is defined as follows:
  \begin{enumerate}

    \item Write \(L_n(m)\) in pure base-\(n + 2\)

    \item Change all \(n + 2\) to \(n + 3\), subtract \(1\)

  \end{enumerate}

\end{itemize}
For \(m = 4\), we have
\[L_0(4) = 4, \ L_1(4) = 26, \ L_2(4) = 41, \ L_3(4) = 60, ..., L_{24}(4) = 1151\]
For \(m = 21\), we have
\[L_0(21) = 21, \ L_1(21) \approxeq 7.6 \cdot 10^{12}, L_2(21) \approxeq 1.3 \cdot 10^{154}, L_3(21) \approxeq 10^{2184}\]
\begin{theorem}
For every \(m \in \nats\), there exists \(k \in \nats\) such that \(L_k(m) = 0\), i.e. the process terminates after a finite number of steps, and \(L(m)\) is a finite sequence.
\label{thm:goodfinite}
\end{theorem}
Pretty shocking, right? In fact, so shocking, that
\begin{theorem}
\(\PA\) cannot prove Theorem \ref{thm:goodfinite}
\end{theorem}
For every \(\alpha \in \LIM\), \(\alpha \leq \epsilon_0\), we are going to define a sequence \(\{C_\alpha(n) : n \in \nats\}\) as follows:
\begin{itemize}

  \item \(C_\omega(n) = n\)

  \item Let \(\alpha = \sum_{i  =1}^k\omega^{\alpha_i}\) with \(\alpha_1 \geq ... \geq \alpha_k\). \(\alpha \in \LIM\), so \(\alpha_k \neq 0\). Let
  \[\gamma = \omega^{\alpha_1} + ... + \omega^{\alpha_{k - 1}} \implies \alpha = \gamma + \omega^{\alpha_k}\]
  \begin{itemize}

    \item If \(\alpha_k\) is a successor, then \(\alpha_k = \beta + 1\), so by continuity
    \[\alpha =
      \gamma + \omega^{\beta + 1} = \gamma + \omega^\beta\omega \implies \alpha
      = \sup\{\gamma + \omega^\beta n : n \in \nats\}\]
    Hence, by ladder (TODO: this)
    \[C_\alpha(n) = \gamma + \omega^\beta n\]

    \item If \(\alpha_k\) is a limit, then
    \[\alpha = \gamma + \omega^\alpha_k = \sup\{\gamma + \omega^{C_n(\alpha_k)} : n \in \omega\}\]
    So by ladder
    \[C_{\alpha}(n) = \gamma + \omega^{C_n(\alpha_k)}\]

  \end{itemize}

\end{itemize}

\end{document}
