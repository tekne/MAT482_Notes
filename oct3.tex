\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT482 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 3 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}
\usepackage{hyperref}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Reg}{reg}
\DeclareMathOperator{\PA}{PA}
\DeclareMathOperator{\PB}{PB}
\DeclareMathOperator{\BM}{BM}
\DeclareMathOperator{\LIM}{LIM}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\osc}{osc}
\DeclareMathOperator{\diam}{diam}

\begin{document}

\maketitle

\section*{Universal Polish Spaces}

We have the following facts:
\begin{itemize}
  \item Every Polish space is a continuous image of \(\nats^\nats\)
  \item Every compact Polish space is a continuous image of \(2^\nats\)
  \item Every separable metric space is homeomorphic to a subpsace of \([0, 1]^\nats\)
  \item Every Polish space is homeomorphic to a closed subpsace of \(\reals^\nats\)
\end{itemize}
These are all classical results, but the corresponding facts for algebraic structures were not known until recently. We begin by discussing the useful notion of a Baire category:
\begin{definition}
  \(N\) is \textbf{nowhere dense} in \(X\) if for all open \(U\), there exists open \(V \subseteq U\) such that \(V \cap N = \varnothing\)
\end{definition}
\begin{definition}
  \(M\) is \textbf{meagre} in \(X\) if \(M = \bigcup_{n = 0}^\infty M_n\) with each \(M_n\) nowhere dense
\end{definition}
\begin{definition}
  \(G\) is \textbf{comeagre} in \(X\) if \(X \setminus G\) is meagre
\end{definition}
Examples include
\begin{itemize}
  \item \(\{x\}\) for \(x \in X\) is nowhere dense if \(x\) is not isolated in \(X\)
  \item \(\rationals\) is meagre in \(\reals\), and hence
  \item The set of irrationals, \(\reals\setminus\rationals\) is comeagre in \(\reals\)
\end{itemize}
\begin{definition}
  \(S \subseteq X\) has the \textbf{property of Baire (PB)} if there is an open set \(U\) such that \(S = U\) modulo meagre, i.e. \(S \Delta U\) is meagre. The family of all PB-subsets of \(X\) is a \(\sigma\)-field of subsets of \(X\).
\end{definition}
It is a fact that if \(f: X \to Y\), where \(X, Y\) are metric and separable, is Baire measurable, i.e.
\(f^{-1}(U)\) has PB for every open \(U \subseteq Y\), then there is a comeager \(G \subseteq X\) such that \(f|_G\) is continuous.
We have that, for \(Z \subseteq Y\) PB, \(Z \Delta U = M\), that
\[f^{-1}(Z) \Delta f^{-1}(U) = f^{-1}(M)\]
\begin{theorem}[Kuradowski-Ulam]
If \(X, Y\) are Polish spaces and \(S \subseteq X \times Y\) has PB, then
\begin{enumerate}
  \item The vertical section \(S_x = \{y : (x, y) \in S\) has PB for a comeager set of \(x \in X\). Similarly for the horizontal section \(S^y = \{x : (x, y) \in S\}\)
  \item \(S\) is meager if and only if \(\{x : S_x \ \text{is meager}\}\) is comeager in \(X\). Similarly for vertical sections.
\end{enumerate}
\end{theorem}
\begin{proof}
Let's check (2) for \(S\) nowhere dense. We may assume that \(S\) is closed. Note that for every \(x \in X\), \(S_x\) is closed in \(Y\). So if \(S_x\) is not meager, it would contain a nonempty open set \(V \in \mc{B}\), where \(\mc{B}\) is a \underline{countable} basis of \(Y\),  such that
\[\{x : S_x \ \text{is not meager in} \ Y\} = \bigcup_{V \in \mc{B} \setminus \ \{\varnothing\}}\{x \in X: S_x \supseteq V\}\]
So if \(Z\) is not meager there is some \(V \in \mc{B} \setminus \{\varnothing\}\) such that \(Z_v = \{x \in X: S_x \supseteq V\}\) is not meager. So \(V \times Z_v \subseteq S\).

\TODOD{this}

\end{proof}

\begin{definition}
  We define the \textbf{Banach-Mazur Game} on a topological space \(X\), \(\BM(X)\), by a sequence \(E = U_0, U_1,...\) and \(V = V_0, V_1,...\) of open sets such that
  \[U_0 \supseteq V_0 \supseteq U_1 \supseteq V_1 \supseteq ...\]
  \(E\) wins the run \((U_0, V_0, U_1, V_1, ...)\) if
  \[\bigcap_{n = 0}^\infty U_n = \varnothing\]
\end{definition}
A strategy for \(E\) could be a function or subtree \(\sigma\) of \(\mc{T}_x^{<\omega}\).

\TODOD{this}

\begin{theorem}
  A space \(X\) is meager in itself if and only if \(E\) has a winning strategy in \(\BM(X)\).
\end{theorem}
\begin{proof}
  Suppose \(X = \bigcup_{n = 0}^\infty X_n\) where \(X_n\) is nowhere dense. We will now construct a strategy tree for \(E\). If \(s \in \sigma\) is of even length \(2n\), find open \(U_n \subseteq \ \text{the last open set in x}\) such that \(U_n \cap X_n = \varnothing\).
  Clearly every branch in \([\sigma]\) has empty intersection.

  For the other direction, we want to construct a dense open set \(G_n \subseteq X\) using a winning strategy \(\sigma\) for \(E\). We'll finish this next time.
\end{proof}

\end{document}
