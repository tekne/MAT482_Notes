\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT482 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 1 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}
\usepackage{hyperref}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Reg}{reg}
\DeclareMathOperator{\PA}{PA}
\DeclareMathOperator{\LIM}{LIM}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\osc}{osc}
\DeclareMathOperator{\diam}{diam}

\begin{document}

\maketitle

In the first assignment, you are asked to find a sentence \(\sigma\) of \(\PA\) such that every model of \(\PA + \sigma\) is nonstandard. Today we're going to begin our second topic: descriptive set theory.

\section*{Descriptive Set Theory}

We will begin with classical descriptive set theory, and then move on to effective (or computational, or recursive) descriptive set theory. So, let's begin

\begin{definition}
  A \textbf{Polish space} is a topological space which is spearable and metrizable with some \underline{complete} metric.
\end{definition}
Recall that a topological space is metrizable when there exists some metric \(d: X^2 \to [0, \infty)\) such that the topology of \(X\) is that induced by the metric, i.e. that generated by the basis of open balls
\[\{B_\epsilon(x) = \{y: d(x, y) < \epsilon\} : x \in X, \epsilon \in \reals^+\}\]
Note that if \(D \subseteq X\) is dense in \(X\) then
\[\{B_\epsilon(x) = \{y: d(x, y) < \epsilon : x \in D, \epsilon \in \rationals^+\}\]
is a countable basis of \(X\). Some examples of Polish spaces include
\begin{itemize}
  \item \(\reals\)
  \item \([0, 1]\)
  \item \(\{0, 1\}^\nats\) - Cantor space
  \item \(\nats^\nats\) - Baire space
  \item \([0, 1]^\nats\) - Hilbert space
  \item \(C(X)\) for compact \(X\)
  \item \(\Aut(M)\) where \(M\) is a countable structure.
\end{itemize}
Suppose \(X\) is Polish and \(Y \subseteq X\) is a subspace. When is \(Y\) polish under the subspace topology? It turns out that this is the case when \(Y\) is of complexity \(G\delta\). Let us try to establish this.

\begin{theorem}
Let \(X\) be a polish space and \(Y \subseteq X\). The following are equivalent:
\begin{enumerate}
  \item \(Y\) is Polish
  \item \(Y\) is \(G\delta\) i.e. the intersection of countably many open subsets of \(X\).
\end{enumerate}
\end{theorem}
We begin by showing \((2) \implies (1)\):
\begin{proof}
Suppose, for \(U_n\) open,
\[Y = \bigcap_{n = 1}^\infty U_n\]
We need to define a complete metric generating the subspace topology on \(Y\). There's a standard trick here: define \(d': Y^2 \to [0, \infty)\) as follows:
\[d'(x, y) = d(x, y) + \sum_{i = 1}^\infty\frac{1}{2^n}\left|\frac{1}{d(x\setminus U_n)} - \frac{1}{d(y, X\setminus U_n)}\right|\]
Of course, this is a metric.

\TODOD{rest}
\end{proof}
We will use the notion of oscillation to complete the above proof. Let \(X\) be a space and \(Y\) be a metric space, and \(f: S \subseteq X \to Y\) be a partial function on \(X\) (defined on \(S\)). For \(x \in X\), we define the oscillation
\[\osc_f(x) = \inf\{\diam(f[U \cap S]) : x \in U\ \text{open}\}\]
For a basis \(\mc{B}\), we can instead write
\[\osc_f(x) = \inf\{\diam(f[U \cap S]) : x \in U \in \mc{B}\}\]
We can define a set
\[G = \{x \in X: \osc_f(x) = 0\}\]
The question is, what kind of set is this?
It is a fact that if \(X\) is separable and metric than \(G\) is \(G\delta\) in \(X\).
To see this, we have to write, for \(U_n\) open in \(X\),
\[G = \bigcap_{n = 1}^\infty U_n\]
Fix a countable basis of \(X\) \(\{B_i: i \in \nats\}\). We can define
\[U_n = \bigcup\{B_i: i \in \nats : \diam(f[B_i \cap S]) < 1/n\}\]
Recall from analysis that not every continuous function on a subspace extends to a continuous functions. However, we have
\begin{theorem}
  Assume \(X\) is metrizable, \(Y\) is completely metrizable, \(S \subseteq X\) and \(f: S \to Y\) is continuous. Then there is a \(G_\delta\)-set \(G\) of \(X\) such that \(S \subseteq G \subseteq \bar{S}\) and a continuous map \(g: G \to Y\) extending \(f\).
\end{theorem}

\subsection*{\(\nats^\nats\) and \(\{0, 1\}^\nats\)}

It turns out that we can translate complexity questions in Polish spaces to these two spaces. The nature of the topology of these spaces, namely open sets being finite sequences elements, means that arithmetic ``knows" about the topology of these spaces, and hence can be used to attack the desired questions.

\begin{definition}
  Let \(S \neq \varnothing\). Define \(S^{<\omega}\) to be the set of finite sequences of elements of \(S\), i.e.
  \[S^{<\omega} = \bigcup_{n \in \omega}S^n\]
  For \(s = (s(0),...,s(n - 1)), t = (t(0),...,t(m - 1))\), we define
  \[s \sqsubseteq t \iff \forall i < n, s(i) = t(i)\]
  This relation extends to \(S^{<\omega} \cup S^\omega\)
\end{definition}
We can think of \(S^{<\omega}\) is a tree, where \(S^\omega\)\ are the infinite branches.
\begin{definition}
  A tree (a subtree of \(S^{<\omega}\) ) is a subset \(T \subseteq S^{<\omega}\) closed downwards under \(\sqsubseteq\).
  \(T\) is called \textbf{pruned} if it has no end nodes, and \textbf{well-founded} if there are no infinite branches. If every node has a finite number of immediate successors, \(T\) is called \textbf{finitely branching}.
\end{definition}

\end{document}
